﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Domain;

namespace Data
{
    public class MRPanelContext : DbContext
    {
        public MRPanelContext(DbContextOptions<MRPanelContext> options) : base(options) { }
        public DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            //See data
            modelBuilder.Entity<Person>().HasData(new Person {Name="Nathan Crum",Family="Crum"});
        }
    }
}
