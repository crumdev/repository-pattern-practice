using System.Threading.Tasks;
using GenericRepository.Interface;

namespace Data
{
    public class UnitOfWork<T> : IUnitOfWork<T> where T : class, IEntity
    {
        private readonly MRPanelContext _context;
        public IRepository<T> Repository { get; }

        public UnitOfWork(MRPanelContext context)
        {
            _context = context;
            Repository = new Repository<T>(context);
        }

        public async Task Commit() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();
    }
}