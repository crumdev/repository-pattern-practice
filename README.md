# Generic Repository Pattern

This code was created following the below tutorial from **Mohammad Reza Pazooki**

## Generic Repository Pattern with UnitOfWork and EF Core in C#

[![Mohammad Reza Pazooki teaching generic repository pattern with UnitOfWork and EF Core in C#](http://img.youtube.com/vi/UhT-I5Z-yoM/0.jpg)](http://www.youtube.com/watch?v=UhT-I5Z-yoM "Mohammad Reza Pazooki Tutorial")