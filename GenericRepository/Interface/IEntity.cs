﻿using System;

namespace GenericRepository.Interface
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
