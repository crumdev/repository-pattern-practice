﻿using System;
using GenericRepository.Interface;

namespace Domain
{
    public abstract class BaseEntity : IEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

    }
}
