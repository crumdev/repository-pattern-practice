using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Data;
using GenericRepository.Interface;

namespace MRPanel.Utility
{
    public static class EfExtensions
    {
        public static IServiceCollection AddEntityFramework(this IServiceCollection services, string user, string host, int port)
        {
            var stringBuilder = new NpgsqlConnectionStringBuilder();
            stringBuilder.Username = user;
            stringBuilder.Host = host;
            stringBuilder.Port = port;
            stringBuilder.Database = "MRPanel";
            services.AddDbContext<MRPanelContext>(options => options.UseNpgsql(stringBuilder.ConnectionString, x => x.MigrationsAssembly("MRPanel")));
            services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            return services;
        }

    }
}